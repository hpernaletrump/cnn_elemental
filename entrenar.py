import sys                                                                         
import os                                                                         
import tensorflow as tf
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator         
from tensorflow.python.keras import optimizers                                 
from tensorflow.python.keras.models import Sequential                              
from tensorflow.python.keras.layers import Dropout, Flatten, Dense, Activation
from tensorflow.python.keras.layers import Convolution2D, MaxPooling2D            
from tensorflow.python.keras import backend as K

K.clear_session()

data_training=r'C:\Users\Hovsep Pernalette\Desktop\Proyecto2\data\training'    
data_validacion=r'C:\Users\Hovsep Pernalette\Desktop\Proyecto2\data\validation' 

epocas = 10 
longitud, altura = 100, 100  
batch_size = 32 
pasos = 1000 
pasos_validacion = 200
filtersConv1 = 32
filtersConv2 = 64 
dim_filter1 = (3,3) 
dim_filter2 = (2,2) 
dim_pool = (2,2)
clases = 8
lr = 0.0004 

## Inicio de Procesamiento de Image

## Variables

training_datagen = ImageDataGenerator(
    rescale=1. / 255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True)

validacion_datagen = ImageDataGenerator(rescale=1. / 255)

image_training=training_datagen.flow_from_directory(
    data_training,
    target_size= (altura, longitud),
    batch_size= batch_size,
    class_mode='categorical'
)

image_validacion = validacion_datagen.flow_from_directory(
    data_validacion,
    target_size= (altura, longitud),
    batch_size= batch_size,
    class_mode='categorical'
)

# indice asociado a cada clase ...
print('indice asociado:',image_validacion.class_indices)
## Desarrollo de la Red Neuronal Convolucional (CNN)

cnn = Sequential()
cnn.add(Convolution2D(filtersConv1, dim_filter1, padding="same", input_shape=(longitud, altura, 3), activation='relu'))
cnn.add(MaxPooling2D(pool_size=dim_pool))

cnn.add(Convolution2D(filtersConv2, dim_filter2, padding="same"))
cnn.add(MaxPooling2D(pool_size=dim_pool))
cnn.add(Flatten())  
cnn.add(Dense(256, activation='relu')) 
cnn.add(Dropout(0.5)) 
cnn.add(Dense(clases, activation='softmax'))

cnn.compile(loss='categorical_crossentropy',
            optimizer=optimizers.Adam(lr=lr),
            metrics=['accuracy'])

cnn.fit_generator(
    image_training,
    steps_per_epoch=pasos,
    epochs=epocas,
    validation_data=image_validacion,
    validation_steps=pasos_validacion)

# Para Guardar el Modelo. Crea el directorio si no existe con el nombre "Model"
dir_target =  r'C:\Users\Hovsep Pernalette\Desktop\Proyecto2\data\model'
if not os.path.exists(dir_target):
    os.mkdir(dir_target)
cnn.save(r'C:\Users\Hovsep Pernalette\Desktop\Proyecto2\data\model\modelo.h5')
cnn.save_weights(r'C:\Users\Hovsep Pernalette\Desktop\Proyecto2\data\model\pesos.h5')
