from django.http import HttpResponse
import datetime
from django.template import Template, Context 

#Contenido Estático / Contenido Dinámico

def opening(request):   #http://localhost:8000/opening/

    testing = ["tes1", "tes2", "tes3", "tes4", "tes5", "tes6", "tes7", "tes8", "tes9", "tes10", "tes11", "tes12", "tes13", "tes14", "tes15", "tes16", "tes17", "tes18"]
    
    date_now = datetime.datetime.now()

    doc_ext = open("C:/Users/Hovsep Pernalette/Desktop/Proyecto2/front_end/plantillas/mod1.html")
    plt = Template(doc_ext.read())
    doc_ext.close()
    ctx = Context({"momento_actual":date_now,"test":testing})
    document = plt.render(ctx)
    return HttpResponse(document)
