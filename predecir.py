import os
import sys
import tensorflow as tf
import numpy as np
import keras
from keras.preprocessing.image import load_img, img_to_array
from tensorflow.keras.models import load_model

altura, longitud = 100, 100
modelo = r'C:\Users\Hovsep Pernalette\Desktop\Proyecto2\data\model\modelo.h5'  
pesos = r'C:\Users\Hovsep Pernalette\Desktop\Proyecto2\data\model\pesos.h5'
cnn = load_model(modelo)
cnn.load_weights(pesos)

def predict(file):
    x = load_img(file, target_size=(longitud, altura))   
    x = img_to_array(x)      
    x = np.expand_dims(x, axis=0)
    arreglo = cnn.predict(x)
    resultado = arreglo[0]
    respuesta = np.argmax(resultado)
    if respuesta == 0:
        print('Se detecta un circulo, un rectangulo y un triangulo')
    elif respuesta == 1:
        print('No se detecta figura')
    elif respuesta == 2:
        print('Se detecta un circulo')
    elif respuesta == 3:
        print('Se detecta un rectangulo')
    elif respuesta == 4:
        print('Se detecta un triangulo')
    elif respuesta == 5:
        print('Se detecta un rectangulo y un circulo')
    elif respuesta == 6:
        print('Se detecta un circulo y un triangulo')
    elif respuesta == 7:
        print('Se detecta un rectangulo y un triangulo')
    return respuesta

predict('tes1.BMP') #Colocar imagen para test #s